# Collections of useful macros for wasm


##  demo
Cargo.toml
```toml
[package]
name = "demo_wasm"
version = "0.1.0"
edition = "2021"

[dependencies]
wasm_macro="0.1.8"
web-sys = { version = "0.3.57", features = [
    'Document',
    'Element',
    'HtmlElement',
    'Node',
    'Window',
    "WindowClient",
] }
dioxus = { version = "0.2.4", features = ["web", "router"] }
console_error_panic_hook = "0.1"
tracing-wasm = "0.2"
tracing = "0.1"
```
index.html
```html
<!-- the same dir in Cargo.toml -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="main"></div>
</body>
</html>
```
main.rs
```rust
fn main(){
    run::launch();
}
pub mod run{
    use dioxus::prelude::*;
    pub fn launch() {
        dioxus::web::launch(app);
    }
    pub fn app(cx: Scope) -> Element {
        use wasm_macro::*;
        console_error_panic_hook::set_once();
        tracing_wasm::set_as_global_default();
        cx.render(rsx! {
            div{
                onclick:move|_|{
                    // location_assign!("/demo");
                    // info!("{}",location_herf!());
                    // location_reload!();
                    let main_div = document_query_selector_all!(".main");
                    match main_div.get(0){ //frist of NodeList
                        Some(div) => {
                            div.append_child(&document_create_element!("div").clone_node().unwrap()).unwrap();
                        },
                        None => {},
                    }
                    console_log!(document_inner_html!(".main"));
                    console_log!(document_base_uri!());
                    document_set_inner_html!(".main","<h1>hello</h1>")
                },
                class:"main",
                "test"
                }
        })
    }
}   
```
then run
```sh
trunk serve
```
## All Examples
```rust
use wasm_macro::*;
//location_assign
onclick:move|_|{
    location_assign!("/demo");// nevigate to /demo
}
//location_herf
onclick:move|_|{
    let herf = location_herf!();
    console_log!(herf); //get current location herf 
}
//location_reload
onclick:move|_|{
    location_reload!(); //reload current location
}
//query_selector
onclick:move|_|{
let main_div = document_query_selector!(".main");
match main_div{
    Some(div) => {
        div.append_child(&document_create_element!("div").clone_node().unwrap());
    },
    None => {},
    }
}
//query_selector_all
onclick:move|_|{
let main_div = document_query_selector_all!(".main");
match main_div.get(0){ //frist of NodeList
    Some(div) => {
        div.append_child(&document_create_element!("div").clone_node().unwrap());
    },
    None => {},
}
// get inner_html
onclick:move|_|{
 console_log!(document_inner_html!(".main"));
}
//set inner_html
onclick:move|_|{
 document_set_inner_html!(".main","<h1>hello</h1>");
}

//console.log()
 onclick:move|_|{
 console_log!(document_inner_html!(".main"));
}

//document_base_uri!() return base_uri
use wasm_macro::*;
onclick:move|_|{
    console_log!(document_base_uri!());
}
use wasm_macro::*;
  onclick:move|_|{
 console_log!(element_get_attribute!(".main","class"));//"main"
}
```