#[allow(warnings)]
#[macro_use]
pub mod macros {
    /// return base_uri
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    ///            console_log!("{:?}",document_base_uri!());
    ///        }
    /// ```
    #[macro_export]
    macro_rules! document_base_uri {
        () => {{
            let window = web_sys::window().expect("no global `window` exists");
            let document = window.document().expect("should have a document on window");
            let uri = document
                .base_uri()
                .expect("should have a base URI on document");
            uri
        }};
    }
    /// location_assign!("/admin") navigate to /admin route
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    ///           location_assign!("/demo");// nevigate to /demo
    ///           location_assign!("/search?q=rust") navigate to /search and with params
    ///        }
    /// ```
    #[macro_export]
    macro_rules! location_assign {
        ($to:expr) => {{
            let window = web_sys::window().expect("no global `window` exists");
            window.location().assign(&format!("{}", $to));
        }};
    }
    ///  get current location herf
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    ///            let herf = location_herf!();
    ///            console_log!("{:?}",herf);
    ///        }
    /// ```
    #[macro_export]
    macro_rules! location_herf {
        () => {{
            let window = web_sys::window().expect("no global `window` exists");
            let herf = window.location().href().unwrap();
            herf
        }};
    }
    /// Reload the resource from the current URL
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    ///            location_reload!();
    ///        }
    /// ```
    #[macro_export]
    macro_rules! location_reload {
        () => {{
            let window = web_sys::window().expect("no global `window` exists");
            window.location().reload().unwrap();
        }};
    }

    ///return first Element within the document that matches the specified selector, or group of selectors
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    ///let main_div = document_query_selector!(".main");
    ///match main_div{
    ///    Some(div) => {
    ///        div.append_child(&document_create_element!("div").clone_node().unwrap());
    ///    },
    ///    None => {},
    ///    }
    ///}
    /// ```
    #[macro_export]
    macro_rules! document_query_selector {
        ($selectors:expr) => {{
            let window = web_sys::window().expect("no global `window` exists");
            let document = window.document().expect("should have a document on window");
            let dom = document
                .query_selector($selectors)
                .expect("failed to find the {$selectors} element");
            dom
        }};
    }
    /// returns a static (not live) NodeList representing a list of the document's elements that match the specified group of selectors.
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    ///let main_div = document_query_selector_all!(".main");
    ///match main_div.get(0){ //frist of NodeList
    ///    Some(div) => {
    ///        div.append_child(&document_create_element!("div").clone_node().unwrap());
    ///    },
    ///    None => {},
    ///}
    /// ```
    #[macro_export]
    macro_rules! document_query_selector_all {
        ($selectors:expr) => {{
            let window = web_sys::window().expect("no global `window` exists");
            let document = window.document().expect("should have a document on window");
            let dom = document
                .query_selector_all($selectors)
                .expect("failed to find the {$selectors} element");
            dom
        }};
    }
    ///Create Element
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    ///let main_div = document_query_selector!(".main");
    ///match main_div{
    ///    Some(div) => {
    ///        div.append_child(&document_create_element!("div").clone_node().unwrap());
    ///    },
    ///    None => {},
    ///    }
    ///}
    /// ```
    #[macro_export]
    macro_rules! document_create_element {
        ($element_name:expr) => {{
            let window = web_sys::window().expect("no global `window` exists");
            let document = window.document().expect("should have a document on window");
            let val = document
                .create_element($element_name)
                .expect("failed to create element `$element_name` on document");
            val
        }};
    }
    ///  get inner_html value of String
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    /// console_log!("{:?}",document_inner_html!(".main"));
    ///}
    /// ```
    #[macro_export]
    macro_rules! document_inner_html {
        ($element_name:expr) => {{
            let element = document_query_selector!($element_name).expect("not font element");
            element.inner_html()
        }};
    }
    ///set inner_html to value
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    /// document_set_inner_html!(".main","<h1>hello</h1>");
    ///}
    /// ```
    #[macro_export]
    macro_rules! document_set_inner_html {
        ($element_name:expr,$val:expr) => {{
            let element = document_query_selector!($element_name).expect("not font element");
            element.set_inner_html($val);
        }};
    }
    ///  log value to browser console
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    /// console_log!("{:?}",document_inner_html!(".main"));
    ///}
    /// ```
    use wasm_bindgen::prelude::*;
    #[wasm_bindgen]
    extern "C" {
        #[wasm_bindgen(js_namespace = console)]
        fn log(s: &str);
    }
    #[macro_export]
    macro_rules! console_log {
        ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
    }
    ///  get element attribute
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    /// console_log!("{:?}",element_get_attribute!(".main","class"));//"main"
    ///}
    /// ```
    #[macro_export]
    macro_rules! element_get_attribute {
        ($element:expr,$attribute:expr) => {{
            let el = document_query_selector!($element).expect("not fontd element");
            el.get_attribute($attribute).expect("not fond the attribute")
        }};
    }

    ///  get element attribute
    /// ```ignore
    /// use wasm_macro::*;
    ///  onclick:move|_|{
    /// console_log!("{:?}",element_get_attribute!(".main","class"));//"main"
    ///}
    /// ```
    #[macro_export]
    macro_rules! demo {
        ($element:expr,$attribute:expr) => {{
            let el = document_query_selector!($element).expect("not fontd element");
            el.get_attribute($attribute).expect("not fond the attribute")
        }};
    }
}

/* 
#[wasm_bindgen(start)]
pub fn main() -> Result<(), JsValue> {
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let body = document.body().expect("document should have a body");
    let toml = include_str!("../Cargo.toml");
    body.set_inner_text(toml);
    Ok(())
}
*/